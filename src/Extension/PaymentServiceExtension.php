<?php

namespace Thunger\SilverStripeOmnipayStripePaymentIntent\Extension;

use Omnipay\Stripe\Message\PaymentIntents\Response;
use Omnipay\Stripe\PaymentIntentsGateway;
use SilverStripe\ORM\DataExtension;

class PaymentServiceExtension extends DataExtension {

	public function updateServiceResponse(&$response) {
		$omnipayResponse = $response->getOmnipayResponse();
		if (
			is_a($omnipayResponse, Response::class)
			&& ($response->isRedirect() || $response->isAwaitingNotification())
			&& $payment = $this->owner->getPayment()
		) {
			$payment->StripePaymentIntentReference = $omnipayResponse->getPaymentIntentReference();
			$payment->write();
		}
	}

}
