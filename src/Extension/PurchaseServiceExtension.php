<?php

namespace Thunger\SilverStripeOmnipayStripePaymentIntent\Extension;

use Omnipay\Stripe\PaymentIntentsGateway;
use SilverShop\Model\Order;
use SilverStripe\ORM\DataExtension;

class PurchaseServiceExtension extends DataExtension {

	public function onBeforePurchase(&$gatewayData) {
		if (isset($gatewayData['stripePaymentMethodID'])) {
			$payment = $this->owner->getPayment();
			$gatewayData['paymentMethod'] = $gatewayData['stripePaymentMethodID'];
			$gatewayData['confirm'] = true;
			if ($order = Order::get()->byID($payment->OrderID)) {
				$gatewayData['description'] = sprintf(
					'#%d, %s, %s',
					$order->ID,
					$order->getName(),
					$order->getLatestEmail()
				);
			}
			$gatewayData['metadata'] = [
				'order_id' => $payment->OrderID,
			];
		}
	}

	public function onBeforeCompletePurchase(&$gatewayData) {
		$payment = $this->owner->getPayment();
		if ($payment->Gateway == 'Stripe\PaymentIntents' && $payment->StripePaymentIntentReference) {
			$gatewayData['paymentIntentReference'] = $payment->StripePaymentIntentReference;
		}
	}

}
