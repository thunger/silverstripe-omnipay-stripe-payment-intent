<?php

namespace Thunger\SilverStripeOmnipayStripePaymentIntent\Extension;

use SilverStripe\ORM\DataExtension;

class PaymentExtension extends DataExtension {

	private static $db = array(
		'StripePaymentIntentReference' => 'Varchar(255)'
	);

}
