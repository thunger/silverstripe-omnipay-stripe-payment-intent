<?php

namespace Thunger\SilverStripeOmnipayStripePaymentIntent;

use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Manifest\ModuleResourceLoader;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Omnipay\GatewayInfo;
use SilverStripe\View\Requirements;
use SilverStripe\View\SSViewer;
use SilverStripe\View\ThemeResourceLoader;

class Helper {
	public static function getStripeFormfields($gatewayKey) {
		$tokenFieldName = 'stripePaymentMethodID';
		$cardFieldName = 'card-element';
		$errorFieldName = 'card-errors';
		$fields = FieldList::create(
			[
				$cardField = LiteralField::create(
					'stripeCardField',
					'<div class="form-group"><div id="'.$cardFieldName.'" class="form-control"></div></div>'
				),
				$errorField = LiteralField::create(
					'stripeErrorField',
					'<div id="'.$errorFieldName.'" role="alert" class="alert alert-warning"></div>'
				),
				HiddenField::create($tokenFieldName, '', ''),
			]
		);

		// Generate a basic config and allow it to be customised
		$stripeConfig = Config::inst()->get(GatewayInfo::class, $gatewayKey);
		$jsConfig = [
			'cardField' => $cardFieldName,
			'errorField' => $errorFieldName,
			'tokenField' => $tokenFieldName,
			'key' => isset($stripeConfig['parameters']) && isset($stripeConfig['parameters']['publishableKey'])
				? $stripeConfig['parameters']['publishableKey']
				: '',
		];

		// add the javascript to the page
		// see https://stripe.com/docs/js/including
		//Requirements::javascript('https://js.stripe.com/v3/');
		Requirements::javascript(
			ModuleResourceLoader::singleton()->resolvePath(
				'thunger/silverstripe-omnipay-stripe-payment-intent:javascript/stripe.js'
			),
			['defer' => true]
		);
		Requirements::customScript("window.StripeConfig = ".json_encode($jsConfig), 'StripeJS');

		return $fields;
	}
}