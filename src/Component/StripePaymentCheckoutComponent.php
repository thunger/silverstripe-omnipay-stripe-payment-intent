<?php

namespace Thunger\SilverStripeOmnipayStripePaymentIntent\Component;

use Omnipay\Stripe\PaymentIntentsGateway;
use SilverShop\Checkout\Checkout;
use SilverShop\Checkout\Component\CheckoutComponent;
use SilverShop\Model\Order;
use SilverStripe\Forms\CompositeField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Omnipay\GatewayInfo;
use SilverStripe\Omnipay\Model\Payment;
use SilverStripe\Omnipay\Service\PurchaseService;
use SilverStripe\ORM\ValidationException;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Security\Security;
use Thunger\SilverStripeOmnipayStripePaymentIntent\Helper;

class StripePaymentCheckoutComponent extends CheckoutComponent {

	protected $isStripe;
	protected $gateway;

	protected function getGateway($order) {
		$method = Checkout::get($order)->getSelectedPaymentMethod(false);
		if ($method && !isset($this->gateway)) {
			$tempPayment = new Payment(
				[
					'Gateway' => $method,
				]
			);
			$service = PurchaseService::create($tempPayment);
			$this->gateway = $service->oGateway();
			$this->isStripe = ($this->gateway instanceof PaymentIntentsGateway);
		}

		return $this->gateway;
	}

	public function setGateway($gateway) {
		$this->gateway = $gateway;
		$this->isStripe = ($this->gateway instanceof PaymentIntentsGateway);
		return $this;
	}

	public function getFormFields(Order $order) {
		$gateway = $this->getGateway($order);

		if (!$order->TotalOutstanding(true)) {
			return FieldList::create();
		}

		$fields = FieldList::create();
		$gateways = GatewayInfo::getSupportedGateways();
		$member = Security::getCurrentUser();

		// add field
		if (count($gateways) > 1) {
			$fields->push(
				OptionsetField::create(
					'PaymentMethod',
					_t("SilverShop\Checkout\CheckoutField.PaymentType", "Payment Type"),
					$gateways,
					array_keys($gateways)
				)->setTemplate('Forms/ButtonOptionsetField')
			);
		}
		if (count($gateways) == 1) {
			$fields->push(
				HiddenField::create('PaymentMethod')->setValue(key($gateways))
			);
		}

		// add custom fields
		$customFields = FieldList::create();
		foreach ($gateways as $key => $title) {
			if ($tmpFields = $this->customCheckoutFields($key)) {
				$customFields->push(
					CompositeField::create($tmpFields)
						->setName($key)
						->addExtraClass('customcheckoutfields customcheckoutfields-'.str_replace('\\', '', $key))
				);
			}
		}
		if ($customFields->Count()) {
			$fields->merge($customFields);
		}

		return $fields;
	}

	public function customCheckoutFields($gatewayKey) {
		$fields = FieldList::create();
		$description = _t('GatewayCheckoutDescription.'.$gatewayKey, $gatewayKey);
		if($description && $description != $gatewayKey) {
			$fields->push(
				LiteralField::create(
					'GatewayCheckoutDescription'.$gatewayKey,
					$description
				)
			);
		}
		switch ($gatewayKey) {
			case 'Stripe':
			case 'Stripe\PaymentIntents':
				$fields->merge(Helper::getStripeFormfields($gatewayKey));
				break;
		}

		return $fields;
	}

	public function getRequiredFields(Order $order) {
		$this->getGateway($order);
		if ($this->isStripe || count(GatewayInfo::getSupportedGateways()) > 1) {
			return [];
		}

		return array('PaymentMethod');
	}

	public function validateData(Order $order, array $data) {
		$this->getGateway($order);
		$result = ValidationResult::create();
		if (!isset($data['PaymentMethod'])) {
			$result->addFieldError(
				'PaymentMethod',
				_t(__CLASS__.'.NoPaymentMethod', "Payment method not provided")
			);
			throw new ValidationException($result);
		}
		$methods = GatewayInfo::getSupportedGateways();
		if (!isset($methods[$data['PaymentMethod']])) {
			$result->addFieldError(
				'PaymentMethod',
				_t(__CLASS__.'.UnsupportedGateway', "Gateway not supported")
			);
			throw new ValidationException($result);
		}

		if ($this->isStripe) {
			// Stripe will validate clientside and if for some reason that falls through
			// it will fail on payment and give an error then. It would be a lot of work to get
			// the token to be namespaced so it could be passed here and there would be no point.
			return true;
		}
	}

	public function getData(Order $order) {
		$this->getGateway($order);
		return array(
			'PaymentMethod' => Checkout::get($order)->getSelectedPaymentMethod(),
		);
	}

	public function setData(Order $order, array $data) {
		$this->getGateway($order);
		if (isset($data['PaymentMethod'])) {
			Checkout::get($order)->setPaymentMethod($data['PaymentMethod']);
		}
	}

	public function providesPaymentData() {
		return true;
	}
}
