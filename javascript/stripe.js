(function (window, document, undefined) {
	'use strict';

	document.addEventListener('DOMContentLoaded', function () {
		var config = window.StripeConfig,
			submitting = false,
			$form = $('form[id^=PaymentForm_]'),
			$methods = $form.find('input[name$=_PaymentMethod]'),
			form = $form.length == 1 ? $form[0] : null,
			$formErrors = $form.find('.alert-dismissable.alert-danger.show'),
			$formErrorContainer = $form.find('#' + $form.attr('id') + '_error'),
			$errorContainer = $form.find('#' + config.errorField).hide(),
			$tokenField = $form.find('input[name*=' + config.tokenField + ']');

		if (!config) {
			console.error('StripeConfig was not set');
			return;
		}

		var stripe = Stripe(config.key);
		var elements = stripe.elements();
		var style = {
			base: {
				color: '#32325d',
				fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
				fontSmoothing: 'antialiased',
				fontSize: '19px',
				'::placeholder': {
					color: '#aab7c4'
				}
			},
			invalid: {
				color: '#fa755a',
				iconColor: '#fa755a'
			}
		};

		// Create an instance of the card Element.
		var card = elements.create('card', {style: style});

		// Add an instance of the card Element into the `card-element` <div>.
		card.mount('#' + config.cardField);

		card.addEventListener('change', function (event) {
			if (event.error) {
				$errorContainer.text(event.error.message).show();
			} else {
				$errorContainer.text('').hide();
				$formErrorContainer.text('').hide();
			}
		});

		$methods.on('change', function () {
			$errorContainer.text('').hide();
			$formErrorContainer.text('').hide();
		});

		// Create a token or display an error when the form is submitted.
		form.addEventListener('submit', function (event) {
			var
				$method = $form.find('input[name$=_PaymentMethod]:checked'),
				submitButton = form.querySelector('.action');
			if(!$method.length) {
				$method = $form.find('input[name$=_PaymentMethod]')
			}
			if (submitButton) {
				submitButton.disabled = true;
			}
			if ($method.val().indexOf('Stripe') === -1) {
				return;
			} else {
				$formErrorContainer.text('').hide();
				$formErrors.alert('close');
			}

			event.preventDefault();
			if (submitting) {
				return false;
			}

			submitting = true;
			stripe.createPaymentMethod({
				type: 'card',
				card: card
			}).then(function (result) {
				if (submitButton) {
					submitButton.disabled = false;
				}
				submitting = false;
				if (result.error) {
					if ($formErrorContainer.length) {
						$formErrorContainer.text(result.error.message).show();
					} else {
						if ($errorContainer.length) {
							$errorContainer.text(result.error.message).show();
						} else {
							console.error(result.error);
						}
					}
				} else {
					$tokenField.val(result.paymentMethod.id);
					$(document).trigger('stripePaymentMethodReceived');
					form.submit();
				}
			});
		});

	});

})(this, this.document);
